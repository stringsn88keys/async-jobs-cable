class JobsBoardsController < ApplicationController
  def index
    @jobs_boards = JobsBoard.all
  end

  def new
    @jobs_board = JobsBoard.new
  end

  def create
    @jobs_board = current_user.jobs_boards.build(jobs_board_params)
    if @jobs_board.save
      flash[:success] = 'Jobs board added!'
    else
      render 'new' and return
    end
    redirect_to jobs_board_path(@jobs_board)
  end

  def show
    @jobs_board = JobsBoard.includes(:postings).find_by(id: params[:id])
    @posting = Posting.new
  end

  private
  def jobs_board_params
    params.require(:jobs_board).permit(:keyword, :description)
  end
end
