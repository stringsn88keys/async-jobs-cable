class JobsBoardsChannel < ApplicationCable::Channel
  def subscribed
    p params
    p "jobs_board_#{params["jobs_board_id"]}_channel"
    stream_from "jobs_board_#{params['jobs_board_id']}_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def send_message(data)
    p data
    current_user.postings.create!(headline: data['title'], body: data['body'], jobs_board_id: data['jobs_board_id'])
  end
end