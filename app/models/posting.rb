# == Schema Information
#
# Table name: postings
#
#  id            :integer          not null, primary key
#  headline      :text
#  body          :text
#  user_id       :integer
#  jobs_board_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_postings_on_headline       (headline)
#  index_postings_on_jobs_board_id  (jobs_board_id)
#  index_postings_on_user_id        (user_id)
#

class Posting < ApplicationRecord
  belongs_to :user
  belongs_to :jobs_board

  validates :headline, presence: true, length: {minimum: 2, maximum: 80}
  validates :body, presence: true, length: {minimum: 2, maximum: 1000}

  after_create_commit { PostingBroadcastJob.perform_later(self) }

  def timestamp
    created_at.strftime('%H:%M:%S %d %B %Y')
  end
end
