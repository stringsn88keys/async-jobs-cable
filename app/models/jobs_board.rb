# == Schema Information
#
# Table name: jobs_boards
#
#  id          :integer          not null, primary key
#  keyword     :string
#  description :string
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_jobs_boards_on_keyword  (keyword)
#  index_jobs_boards_on_user_id  (user_id)
#

class JobsBoard < ApplicationRecord
  belongs_to :user
  has_many :postings, dependent: :destroy

  validates_presence_of :keyword, :description
end
