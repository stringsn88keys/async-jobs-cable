class PostingBroadcastJob < ApplicationJob
  queue_as :default

  def perform(posting)
    ActionCable.server.broadcast "jobs_board_#{posting.jobs_board.id}_channel",
        message: render_posting(posting)
  end

  private
  def render_posting(posting)
    PostingController.render partial: 'postings/posting', locals: { posting: posting }
  end
end