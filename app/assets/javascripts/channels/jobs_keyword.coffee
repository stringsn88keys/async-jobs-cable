jQuery(document).on 'turbolinks:load', ->
  postings = $('#postings')
  if $('#postings').length > 0
    App.global_chat = App.cable.subscriptions.create {
      channel: "JobsBoardsChannel"
      jobs_board_id: postings.data('jobs-board-id')
    },
      connected: ->
        $('#connection_status').html('[CONNECTED]')
        $('#connection_status').addClass('text-success')
        $('#connection_status').addClass('bg-success')
        $('#connection_status').removeClass('text-danger')
        $('#connection_status').removeClass('bg-danger')
        $('#connection_status').removeClass('text-muted')
#        console.log(postings.data('jobs-board-id'))
#        console.log('connected')
# Called when the subscription is ready for use on the server

      disconnected: ->
        $('#connection_status').html('[DISCONNECTED]')
        $('#connection_status').addClass('text-danger')
        $('#connection_status').addClass('bg-danger')
        $('#connection_status').removeClass('text-success')
        $('#connection_status').addClass('bg-success')
        $('#connection_status').removeClass('text-muted')
# Called when the subscription has been terminated by the server

      received: (data) ->
        postings.prepend data['message']

      send_message: (title, body, jobs_board_id) ->
        @perform 'send_message', title: title, body: body, jobs_board_id: jobs_board_id


    $('#new_posting').submit (e) ->
      $this = $(this)
      headline = $this.find('#posting_headline')
      body = $this.find('#posting_body')
      if $.trim(headline.val()).length > 1 && $.trim(body.val()).length > 1
        App.global_chat.send_message headline.val(), body.val(), postings.data('jobs-board-id')
        headline.val('')
        body.val('')
      e.preventDefault()
      return false