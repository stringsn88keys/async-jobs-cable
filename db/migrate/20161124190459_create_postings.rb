class CreatePostings < ActiveRecord::Migration[5.0]
  def change
    create_table :postings do |t|
      t.text :headline
      t.text :body
      t.references :user, foreign_key: true
      t.references :jobs_board, foreign_key: true

      t.timestamps
    end
    add_index :postings, :headline
  end
end
