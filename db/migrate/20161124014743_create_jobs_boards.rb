class CreateJobsBoards < ActiveRecord::Migration[5.0]
  def change
    create_table :jobs_boards do |t|
      t.string :keyword
      t.string :description
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :jobs_boards, :keyword
  end
end
